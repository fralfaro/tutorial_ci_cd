import pytest
from example.main import sumar


@pytest.mark.parametrize("x,y,expected", [(1, 1, 2), (2, 1, 3), (1, 2, 3)])
def test_suma(x, y, expected):
    result = sumar(x, y)
    assert result == expected
